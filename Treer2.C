void Treer2(){
  

  TFile* f219 = TFile::Open("/Users/nakkalil/cernbox/athena_clones/21.9_MyAthenaWorkingFolder/run/PixelRDOAnalysis.root");
  if(f219 == 0) {
    std::cout << " Error! Cannot open the root file " <<std::endl;
  }

  TTree* tree_219 = (TTree*)f219->Get("PixelRDOAnalysis/PixelRDOAna");
  double  charge;
  //  TBranch* b_charge = tree_219->GetBranch("charge");
  // b_charge->SetAddress(&charge);

  tree_219->SetBranchAddress("charge",&charge);

  TH1D* h_charge = new TH1D("h_charge", "Charge distribution in 21.9", 100,0,1e7);

  auto nentries = tree_219->GetEntries();
  for ( int i = 0; i < nentries ; i++) {
    tree_219->GetEntry(i);
    h_charge->Fill(charge);
  }
  //tree_219->Project("h_charge","charge");
  
  TCanvas* c = new TCanvas();

  h_charge->Draw("charge");
  
}
  
