void tree_ratio(){

  TFile* f219 = TFile::Open("/Users/nakkalil/cernbox/athena_clones/21.9_MyAthenaWorkingFolder/run/PixelRDOAnalysis.root");
  if(f219 == 0) {
    std::cout << " Error! Cannot open the root file " <<std::endl;
  }

  TFile* f22 = TFile::Open("/Users/nakkalil/cernbox/athena_clones/Athena08022022/run/1000Events_ITkPixelRDOAnalysis.root");
  if(f22 == 0) {
    std::cout << " Error! Cannot open the root file " <<std::endl;
  }

  TH1D* h_ToT_R219 = new TH1D("h_ToT_R219"," ", 20, 0, 20);
  TH1D* h_ToT_R22 = new TH1D("h_ToT_R22"," ", 20, 0,20);
  
  TTree* tree_219 = (TTree*)f219->Get("PixelRDOAnalysis/PixelRDOAna");
  TTree* tree_22 = (TTree*)f22->Get("ITkPixelRDOAnalysis/ITkPixelRDOAnalysis");

  tree_219->Project("h_ToT_R219","ToT","barrelEndcap==0 && layerDisk==4");
  tree_22->Project("h_ToT_R22","ToT","barrelEndcap==0 && layerDisk==4");
  
  TCanvas* c1 = new TCanvas();
  //  c->Divide(1,2);
  //  c1->SetLogy();

  h_ToT_R219->SetLineColor(kBlue);
  h_ToT_R219->GetYaxis()->SetRangeUser(0,400);
  h_ToT_R219->SetTitle("ToT (RDO) distribution - layer 4");
  h_ToT_R219->GetXaxis()->SetTitle("ToT");
  h_ToT_R219->Draw();
  h_ToT_R22->SetLineColor(kRed);
  // h_charge_R22->Draw("sames");
   //   c->cd(1);
  
  /* tree_219->SetLineColor(kBlue);
  tree_219->Draw("charge","abs(barrelEndcap_sdo)==2 && layerDisk_sdo==7");
  TH1D* htemp = (TH1D*)gPad->GetPrimitive("htemp");
  htemp->SetTitle("Endcap charge (SDO) distribution - layer 7");
  tree_22->SetLineColor(kRed);
  tree_22->Draw("charge","abs(barrelEndcap_sdo)==2 && layerDisk_sdo==7", "sames");
  //  tree_219->Draw("charge","barrelEndcap_sdo==0 && layerDisk_sdo==0");

  TLegend *legend = new TLegend(0.55,0.65,0.76,0.82);
  legend->AddEntry(tree_219,"R21.9","l");
  legend->AddEntry(tree_22,"R22","l");
  legend->Draw();*/

  auto rp = new TRatioPlot(h_ToT_R219,h_ToT_R22);
  c1->SetTicks(0,1);
  rp->SetH2DrawOpt("hist");
  rp->Draw();
  c1->Update();

  rp->GetUpperPad()->cd();
  h_ToT_R22->Draw("sames"); 
  //h2->Draw("][sames");

  rp->GetLowerRefGraph()->SetMinimum(0.5);
  rp->GetLowerRefGraph()->SetMaximum(2.0);
  rp->GetLowerRefYaxis()->SetTitle("R21.9/R22");
  rp->GetLowerRefYaxis()->SetRangeUser(0.2,5);
  rp->GetUpperRefYaxis()->SetTitle("entries");
  rp->GetUpperPad()->Update();
  
  TPaveStats *ps1 = (TPaveStats*)h_ToT_R219->GetListOfFunctions()->FindObject("stats");
  ps1->SetX1NDC(0.65); ps1->SetX2NDC(0.85);
  ps1->SetTextColor(kBlue);
  ps1->SetLineColor(kBlue);
  TPaveStats *ps2 = (TPaveStats*)h_ToT_R22->GetListOfFunctions()->FindObject("stats");
  ps2->SetX1NDC(0.65); ps2->SetX2NDC(0.85);
  ps2->SetY1NDC(0.25); ps2->SetY2NDC(0.45);
  ps2->SetTextColor(kRed);
  ps2->SetLineColor(kRed);


}
  BB
