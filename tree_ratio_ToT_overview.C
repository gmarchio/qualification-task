void tree_ratio_ToT_overview(){

  TFile* f219 = TFile::Open("/Users/nakkalil/cernbox/athena_clones/21.9_MyAthenaWorkingFolder/run/PixelRDOAnalysis.root");
  if(f219 == 0) {
    std::cout << " Error! Cannot open the root file " <<std::endl;
  }

  TFile* f22 = TFile::Open("/Users/nakkalil/cernbox/athena_clones/Athena08022022/run/1000Events_ITkPixelRDOAnalysis.root");
  if(f22 == 0) {
    std::cout << " Error! Cannot open the root file " <<std::endl;
  }

  TH1D* h1_R219 = new TH1D("h1_R219"," Layer 0 ", 20, 0, 20); // 
  TH1D* h2_R22 = new TH1D("h2_R22"," ", 20,0,20);

  TH1D* h3_R219 = new TH1D("h3_R219"," Layer 1 ", 20, 0, 20);
  TH1D* h4_R22 = new TH1D("h4_R22"," ", 20, 0,20);

  TH1D* h5_R219 = new TH1D("h5_R219","Layer 2 ", 20, 0, 20);
  TH1D* h6_R22 = new TH1D("h6_R22"," ", 20, 0,20);

  TH1D* h7_R219 = new TH1D("h7_R219"," Layer 3 ", 20, 0, 20);
  TH1D* h8_R22 = new TH1D("h8_R22"," ", 20, 0, 20);

  TH1D* h9_R219 = new TH1D("h9_R219","Layer 4 ", 20, 0, 20);
  TH1D* h10_R22 = new TH1D("h10_R22"," ", 20, 0, 20);
  
  TTree* tree_219 = (TTree*)f219->Get("PixelRDOAnalysis/PixelRDOAna");
  TTree* tree_22 = (TTree*)f22->Get("ITkPixelRDOAnalysis/ITkPixelRDOAnalysis");

  tree_219->Project("h1_R219","ToT","barrelEndcap==0 && layerDisk==0");
  tree_22->Project("h2_R22","ToT","barrelEndcap==0 && layerDisk==0");

  tree_219->Project("h3_R219","ToT","barrelEndcap==0 && layerDisk==1");
  tree_22->Project("h4_R22","ToT","barrelEndcap==0 && layerDisk==1");

  tree_219->Project("h5_R219","ToT","barrelEndcap==0 && layerDisk==2");
  tree_22->Project("h6_R22","ToT","barrelEndcap==0 && layerDisk==2");

  tree_219->Project("h7_R219","ToT","barrelEndcap==0 && layerDisk==3");
  tree_22->Project("h8_R22","ToT","barrelEndcap==0 && layerDisk==3");

  tree_219->Project("h9_R219","ToT","barrelEndcap==0 && layerDisk==4");
  tree_22->Project("h10_R22","ToT","barrelEndcap==0 && layerDisk==4");
  
  TCanvas* c1 = new TCanvas();
  c1->Divide(3,2);
  //  c1->SetLogy();

  c1->cd(1);
  h1_R219->SetLineColor(kBlue);
  h1_R219->GetYaxis()->SetRangeUser(0,2400);
  // h1_ToT_R219->SetTitle("ToT (RDO) distribution - layer 0");
  h1_R219->GetXaxis()->SetTitle("ToT");
  h1_R219->Draw();
  //  c1->Update();
  h2_R22->SetLineColor(kRed);
 
  auto rp = new TRatioPlot(h1_R219,h2_R22);
  c1->SetTicks(0,1);
  rp->SetH2DrawOpt("hist");
  rp->Draw();
  c1->Update();

  rp->GetUpperPad()->cd();
  h2_R22->Draw("sames"); 
  
  rp->GetLowerRefYaxis()->SetTitleOffset(1.2);
  rp->GetUpperRefYaxis()->SetTitleOffset(1.6);
  rp->GetLowerRefGraph()->SetMinimum(0.5);
  rp->GetLowerRefGraph()->SetMaximum(2.0);
  rp->GetLowerRefYaxis()->SetTitle("R21.9/R22");
  rp->GetLowerRefYaxis()->SetRangeUser(0.2,2.5);
  rp->GetUpperRefYaxis()->SetTitle("entries");
  rp->GetUpperPad()->Update();
  
  TPaveStats *ps1 = (TPaveStats*)h1_R219->GetListOfFunctions()->FindObject("stats");
  ps1->SetX1NDC(0.65); ps1->SetX2NDC(0.85);
  ps1->SetTextColor(kBlue);
  ps1->SetLineColor(kBlue);
  TPaveStats *ps2 = (TPaveStats*)h2_R22->GetListOfFunctions()->FindObject("stats");
  ps2->SetX1NDC(0.65); ps2->SetX2NDC(0.85);
  ps2->SetY1NDC(0.25); ps2->SetY2NDC(0.45);
  ps2->SetTextColor(kRed);
  ps2->SetLineColor(kRed);

  TLegend* leg1 = new TLegend(0.6,0.7,0.9,0.9);
  leg1->AddEntry(h1_R219,"R21.9");
  leg1->AddEntry(h2_R22,"R22");
  leg1->Draw();

  c1->cd(2);
  h3_R219->SetLineColor(kBlue);
  h3_R219->GetYaxis()->SetRangeUser(0,1300);
  h3_R219->GetXaxis()->SetTitle("ToT");
  h3_R219->Draw();
  h4_R22->SetLineColor(kRed);

  auto rp2 = new TRatioPlot(h3_R219,h4_R22);
  c1->SetTicks(0,1);
  rp2->SetH2DrawOpt("hist");
  rp2->Draw();
  c1->Update();

  rp2->GetUpperPad()->cd();
  h4_R22->Draw("sames");


  rp2->GetLowerRefYaxis()->SetTitleOffset(1.2);
  rp2->GetUpperRefYaxis()->SetTitleOffset(1.6);
  rp2->GetLowerRefGraph()->SetMinimum(0.5);
  rp2->GetLowerRefGraph()->SetMaximum(2.0);
  rp2->GetLowerRefYaxis()->SetTitle("R21.9/R22");
  rp2->GetLowerRefYaxis()->SetRangeUser(0.2,2.5);
  rp2->GetUpperRefYaxis()->SetTitle("entries");
  rp2->GetUpperPad()->Update();

  TPaveStats *ps3 = (TPaveStats*)h3_R219->GetListOfFunctions()->FindObject("stats");
  ps3->SetX1NDC(0.65); ps3->SetX2NDC(0.85);
  ps3->SetTextColor(kBlue);
  ps3->SetLineColor(kBlue);
  TPaveStats *ps4 = (TPaveStats*)h4_R22->GetListOfFunctions()->FindObject("stats");
  ps4->SetX1NDC(0.65); ps4->SetX2NDC(0.85);
  ps4->SetY1NDC(0.25); ps4->SetY2NDC(0.45);
  ps4->SetTextColor(kRed);
  ps4->SetLineColor(kRed);

  TLegend* leg2 = new TLegend(0.6,0.7,0.9,0.9);
  leg2->AddEntry(h3_R219,"R21.9");
  leg2->AddEntry(h4_R22,"R22");
  leg2->Draw();

  c1->cd(3);
  h5_R219->SetLineColor(kBlue);
  h5_R219->GetYaxis()->SetRangeUser(0,2400);
  h5_R219->GetXaxis()->SetTitle("ToT");
  h5_R219->Draw();
  h6_R22->SetLineColor(kRed);

  auto rp3 = new TRatioPlot(h5_R219,h6_R22);
  c1->SetTicks(0,1);
  rp3->SetH2DrawOpt("hist");
  rp3->Draw();
  c1->Update();

  rp3->GetUpperPad()->cd();
  h6_R22->Draw("sames");


  rp3->GetLowerRefYaxis()->SetTitleOffset(1.2);
  rp3->GetUpperRefYaxis()->SetTitleOffset(1.6);
  rp3->GetLowerRefGraph()->SetMinimum(0.5);
  rp3->GetLowerRefGraph()->SetMaximum(2.0);
  rp3->GetLowerRefYaxis()->SetTitle("R21.9/R22");
  rp3->GetLowerRefYaxis()->SetRangeUser(0.2,5);
  rp3->GetUpperRefYaxis()->SetTitle("entries");
  rp3->GetUpperPad()->Update();

  TPaveStats *ps5 = (TPaveStats*)h5_R219->GetListOfFunctions()->FindObject("stats");
  ps5->SetX1NDC(0.65); ps5->SetX2NDC(0.85);
  ps5->SetTextColor(kBlue);
  ps5->SetLineColor(kBlue);
  TPaveStats *ps6 = (TPaveStats*)h6_R22->GetListOfFunctions()->FindObject("stats");
  ps6->SetX1NDC(0.65); ps6->SetX2NDC(0.85);
  ps6->SetY1NDC(0.25); ps6->SetY2NDC(0.45);
  ps6->SetTextColor(kRed);
  ps6->SetLineColor(kRed);

  TLegend* leg3 = new TLegend(0.6,0.7,0.9,0.9);
  leg3->AddEntry(h5_R219,"R21.9");
  leg3->AddEntry(h6_R22,"R22");
  leg3->Draw();

  c1->cd(4);
  h7_R219->SetLineColor(kBlue);
  h7_R219->GetYaxis()->SetRangeUser(0,1300);
  h7_R219->GetXaxis()->SetTitle("ToT");
  h7_R219->Draw();
  h8_R22->SetLineColor(kRed);

  auto rp4 = new TRatioPlot(h7_R219,h8_R22);
  c1->SetTicks(0,1);
  rp2->SetH2DrawOpt("hist");
  rp4->Draw();
  c1->Update();

  rp4->GetUpperPad()->cd();
  h8_R22->Draw("sames");


  rp4->GetLowerRefYaxis()->SetTitleOffset(1.2);
  rp4->GetUpperRefYaxis()->SetTitleOffset(1.6);
  rp4->GetLowerRefGraph()->SetMinimum(0.5);
  rp4->GetLowerRefGraph()->SetMaximum(2.0);
  rp4->GetLowerRefYaxis()->SetTitle("R21.9/R22");
  rp4->GetLowerRefYaxis()->SetRangeUser(0.2,2.5);
  rp4->GetUpperRefYaxis()->SetTitle("entries");
  rp4->GetUpperPad()->Update();

  TPaveStats *ps7 = (TPaveStats*)h7_R219->GetListOfFunctions()->FindObject("stats");
  ps7->SetX1NDC(0.65); ps7->SetX2NDC(0.85);
  ps7->SetTextColor(kBlue);
  ps7->SetLineColor(kBlue);
  TPaveStats *ps8 = (TPaveStats*)h8_R22->GetListOfFunctions()->FindObject("stats");
  ps8->SetX1NDC(0.65); ps8->SetX2NDC(0.85);
  ps8->SetY1NDC(0.25); ps8->SetY2NDC(0.45);
  ps8->SetTextColor(kRed);
  ps8->SetLineColor(kRed);

  TLegend* leg4 = new TLegend(0.6,0.7,0.9,0.9);
  leg4->AddEntry(h7_R219,"R21.9");
  leg4->AddEntry(h8_R22,"R22");
  leg4->Draw();

  c1->cd(5);
  h9_R219->SetLineColor(kBlue);
  h9_R219->GetYaxis()->SetRangeUser(0,2400);
  h9_R219->GetXaxis()->SetTitle("ToT");
  h9_R219->Draw();
  h10_R22->SetLineColor(kRed);

  auto rp5 = new TRatioPlot(h9_R219,h10_R22);
  c1->SetTicks(0,1);
  rp5->SetH2DrawOpt("hist");
  rp5->Draw();
  c1->Update();

  rp5->GetUpperPad()->cd();
  h10_R22->Draw("sames");


  rp5->GetLowerRefYaxis()->SetTitleOffset(1.2);
  rp5->GetUpperRefYaxis()->SetTitleOffset(1.6);
  rp5->GetLowerRefGraph()->SetMinimum(0.5);
  rp5->GetLowerRefGraph()->SetMaximum(2.0);
  rp5->GetLowerRefYaxis()->SetTitle("R21.9/R22");
  rp5->GetLowerRefYaxis()->SetRangeUser(0.2,2.5);
  rp5->GetUpperRefYaxis()->SetTitle("entries");
  rp5->GetUpperPad()->Update();

  TPaveStats *ps9 = (TPaveStats*)h9_R219->GetListOfFunctions()->FindObject("stats");
  ps9->SetX1NDC(0.65); ps9->SetX2NDC(0.85);
  ps9->SetTextColor(kBlue);
  ps9->SetLineColor(kBlue);
  TPaveStats *ps10 = (TPaveStats*)h10_R22->GetListOfFunctions()->FindObject("stats");
  ps10->SetX1NDC(0.65); ps10->SetX2NDC(0.85);
  ps10->SetY1NDC(0.25); ps10->SetY2NDC(0.45);
  ps10->SetTextColor(kRed);
  ps10->SetLineColor(kRed);

  TLegend* leg5 = new TLegend(0.6,0.7,0.9,0.9);
  leg5->AddEntry(h9_R219,"R21.9");
  leg5->AddEntry(h10_R22,"R22");
  leg5->Draw();



}
  
